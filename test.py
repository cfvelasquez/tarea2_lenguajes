import base64
import json
from io import BytesIO

import googlemaps
import requests
from PIL import Image

from flask import Flask, jsonify, request

app = Flask(__name__)

def ejercicio1(origen, destino):
    g_client = googlemaps.Client(key='AIzaSyDBQESnSAY-HlvtjUQD7JO4uIg6B9UShXE')

    data = g_client.directions(origen,destino)

    json_data = data[0]['legs'][0]['steps']

    lat = []
    lng = []

    for x in xrange(len(json_data)):
        lat.append(str(data[0]['legs'][0]['steps'][x]['start_location']['lat']))
        lng.append(str(data[0]['legs'][0]['steps'][x]['start_location']['lng']))
        lat.append(str(data[0]['legs'][0]['steps'][x]['end_location']['lat']))
        lng.append(str(data[0]['legs'][0]['steps'][x]['end_location']['lng']))

    return lat,lng


def ejercicio2(origen):
    g_client = googlemaps.Client(key='AIzaSyDBQESnSAY-HlvtjUQD7JO4uIg6B9UShXE')

    data = g_client.geocode(origen)

    lat = data[0]['geometry']['location']['lat']
    lng = data[0]['geometry']['location']['lng']

    place_data = g_client.places_nearby((lat,lng),2350,type='restaurant')

    json_data = place_data['results']

    nombre = []
    lat = []
    lng = [] 

    for x in xrange(len(json_data)):
        nombre.append(place_data['results'][x]['name'])
        lat.append(str(place_data['results'][x]['geometry']['location']['lat']))
        lng.append(str(place_data['results'][x]['geometry']['location']['lng']))

    return nombre,lat,lng


def ejercicio3(bmp_name):

    img = Image.open(bmp_name)

    width, height = img.size

    newImage = Image.new('RGB', (width,height))
    pixels = newImage.load()

    for x in range(width):
        for y in range(height):
            pixels = img.getpixel((x,y))
            R,G,B = pixels

            average = (R+G+B)/3

            R = average
            G = average
            B = average

            newPixels = R,G,B 

            newImage.putpixel((x,y),(newPixels))


    newImage.save(bmp_name)

    with open(bmp_name, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    return encoded_string

def B64ToIMG(name, data):

    img = Image.open(BytesIO(base64.b64decode(data)))

    bmp_name = name 

    img.save(bmp_name, 'BMP')

    return bmp_name


@app.route('/ejercicio1', methods=['POST'])
def post_ejercicio1():
    try:
        origen = request.json['origen']
        destino = request.json['destino']
    except:
        err={
            "Error": "no se especifico origen"
        }
        return jsonify(err), 400


    lat,lng = ejercicio1(origen,destino)

    res = {};
    for x in xrange( 0, len( lat ) ):
        res[x] = {
            "lat": lat [x],
            "lng": lng[x]
        }

    gmaps={
        "rutas": res,
    }

    return jsonify(gmaps), 201

@app.route('/ejercicio2', methods=['POST'])
def post_ejercicio2():
    try:
        origen = request.json['origen']
    except:
        err={
            "Error": "no se especifico origen"
        }
        return jsonify(err), 400

    nombre,lat,lng = ejercicio2(origen)

    
    res = {};
    for x in xrange( 0, len( nombre ) ):
        res[nombre[x]] = {
            "lat": lat[x],
            "lng": lng[x]
        }

    gmaps={
        "restaurantes": res
    }
    return jsonify(gmaps), 201

@app.route('/ejercicio3', methods=['POST'])
def post_ejercicio3():
    try:
        nombre = request.json['nombre']
        data = request.json['data']
    except:
        err={
            "Error": "no se especifico origen"
        }
        return jsonify(err), 400

    bmp_name = B64ToIMG(nombre,data)
    data = ejercicio3(bmp_name)

    imagen={
    "nombre": nombre,
    "data": data 
    }

    return jsonify(imagen), 201

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)